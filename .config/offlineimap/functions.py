from subprocess import check_output

def get_pass(acct):
    return bytes.decode(check_output("secret-tool lookup Title email-" + acct, shell=True)).strip("\n")
