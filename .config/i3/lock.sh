#!/bin/sh

dunstctl set-paused true

xscreensaver-command -lock

while [ -n "$(xscreensaver-command -time | grep locked)" ]
do
    echo Locked
    sleep 1
done
echo Unlocked!
dunstctl set-paused false
