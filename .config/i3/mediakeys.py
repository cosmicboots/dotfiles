#!/usr/bin/python

from subprocess import check_output, run
from sys import argv

print(argv)

key_command = "toggle"
if len(argv) > 1: key_command = argv[1]
print(key_command)

def command_parse(cmd):
    return check_output(cmd, shell = True).decode().strip('\n')

def get_current():
    return command_parse("mpc current")

def get_next():
    return command_parse("mpc queued")

if (get_current() or get_next()):
    run("mpc " + key_command, shell = True)
else:
    print("No MPD Songs. Using spotify")
    spt_cmd = ""
    if(key_command == "toggle"):
        spt_cmd = "PlayPause"
    elif(key_command == "next"):
        spt_cmd = "Next"
    elif(key_command == "prev"):
        spt_cmd = "Previous"
    run("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player." + spt_cmd, shell = True)
