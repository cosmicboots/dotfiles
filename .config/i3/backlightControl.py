import os
import sys

currentLight = float(os.popen("xbacklight -get").read().split('\n')[0])
#currentLight = 0

args = sys.argv
if len(args) == 1:
    print("No args provided")
    sys.exit()
args.pop(0)

if args[0] == "down":
    if currentLight > 10:
        os.system("xbacklight -10")
    else:
        os.system("xbacklight -set 1")
elif args[0] == "up":
    os.system("xbacklight +10")
else:
    print("Unknown args")

#print(sys.argv)
