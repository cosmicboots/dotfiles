#!/bin/sh

LOG=$(mktemp -t i3-start-log-XXXX)

i=0

echo > $LOG

# Wait for keepass to unlock
keepassxc &
while [ "$(secret-tool search Title dummy)" = "" ]
do
    echo KeePass not unlocked >> $LOG
    sleep 1
done

# Append layouts to each workspace
for item in "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9" "${10}"
do
    i=$(( i + 1 ))
    echo Moving to workspace: $item >> $LOG
    echo Workspace index: $i >> $LOG
    i3-msg "workspace $item"
    i3-msg "append_layout ~/.config/i3/workspaces/$i.json"
done

# Move back to the first workspace
i3-msg "workspace $1"

# Launch remaining applications
echo "Starting applications" >> $LOG
nm-applet &
telegram-desktop &
#gajim &
#discord &
firefox &
nextcloud &
flameshot &

# Start mpdscribble
echo "Starting mpdscribble" >> $LOG
systemctl --user start mpdscribble.service >> $LOG

echo "Startup script finished" >> $LOG

