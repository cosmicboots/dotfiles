local wezterm = require("wezterm")
local act = wezterm.action

local config = wezterm.config_builder()

config.color_scheme = "carbonfox"
config.font = wezterm.font("DMMono Nerd Font")

config.disable_default_key_bindings = true
config.hide_tab_bar_if_only_one_tab = false
config.use_fancy_tab_bar = false
config.tab_bar_at_bottom = false

config.ssh_backend = "LibSsh"

config.window_padding = {
    left = '1cell',
    right = '1cell',
    top = '0.5cell',
    bottom = '0.5cell',
}

wezterm.on('update-right-status', function(window, pane)
    local date = wezterm.strftime '%Y-%m-%d %H:%M:%S'
    local leader_status
    if window:leader_is_active() then
        leader_status = "<Leader> "
    else
        leader_status = ""
    end

    -- Make it italic and underlined
    window:set_right_status(wezterm.format {
        { Attribute = { Underline = 'Single' } },
        { Attribute = { Italic = true } },
        { Text = leader_status .. date },
    })
end)


config.leader = { key = "b", mods = "ALT", timeout_milliseconds = 5000 }

config.keys = {
    -- Tab navigation
    { key = "t",     mods = "ALT",          action = act.SpawnTab "CurrentPaneDomain" },
    { key = "n",     mods = "ALT",          action = act.ActivateTabRelative(1) },
    { key = "p",     mods = "ALT",          action = act.ActivateTabRelative(-1) },
    { key = "0",     mods = "ALT",          action = act.ActivateTab(0) },
    { key = "1",     mods = "ALT",          action = act.ActivateTab(1) },
    { key = "2",     mods = "ALT",          action = act.ActivateTab(2) },
    { key = "3",     mods = "ALT",          action = act.ActivateTab(3) },
    { key = "4",     mods = "ALT",          action = act.ActivateTab(4) },
    { key = "5",     mods = "ALT",          action = act.ActivateTab(5) },
    { key = "6",     mods = "ALT",          action = act.ActivateTab(6) },
    { key = "7",     mods = "ALT",          action = act.ActivateTab(7) },
    { key = "8",     mods = "ALT",          action = act.ActivateTab(8) },
    { key = "9",     mods = "ALT",          action = act.ActivateTab(9) },

    { key = "F11",   mods = "NONE",         action = act.ToggleFullScreen },

    -- Pane navigation
    { key = "%",     mods = "LEADER|SHIFT", action = act.SplitHorizontal { domain = "CurrentPaneDomain" } },
    { key = "Enter", mods = "ALT",          action = act.SplitHorizontal { domain = "CurrentPaneDomain" } },
    { key = "\"",    mods = "LEADER|SHIFT", action = act.SplitVertical { domain = "CurrentPaneDomain" } },
    { key = "Enter", mods = "ALT|SHIFT",    action = act.SplitVertical { domain = "CurrentPaneDomain" } },
    { key = "z",     mods = "ALT",          action = act.TogglePaneZoomState },
    { key = "h",     mods = "ALT",          action = act.ActivatePaneDirection "Left" },
    { key = "H",     mods = "SHIFT|ALT",    action = act.AdjustPaneSize { "Left", 1 } },
    { key = "l",     mods = "ALT",          action = act.ActivatePaneDirection "Right" },
    { key = "L",     mods = "SHIFT|ALT",    action = act.AdjustPaneSize { "Right", 1 } },
    { key = "k",     mods = "ALT",          action = act.ActivatePaneDirection "Up" },
    { key = "K",     mods = "SHIFT|ALT",    action = act.AdjustPaneSize { "Up", 1 } },
    { key = "j",     mods = "ALT",          action = act.ActivatePaneDirection "Down" },
    { key = "J",     mods = "SHIFT|ALT",    action = act.AdjustPaneSize { "Down", 1 } },
    { key = "w",     mods = "ALT",          action = act.CloseCurrentPane { confirm = true } },
    { key = "r",     mods = "ALT",          action = act.RotatePanes "Clockwise" },
    {
        key = '!',
        mods = 'LEADER | SHIFT',
        action = wezterm.action_callback(function(win, pane)
            local tab, _ = pane:move_to_new_tab()
            tab:activate()
        end),
    },


    -- Zoom settings
    { key = "=", mods = "CTRL",       action = act.IncreaseFontSize },
    { key = "-", mods = "CTRL",       action = act.DecreaseFontSize },
    { key = "0", mods = "CTRL",       action = act.ResetFontSize },

    { key = "C", mods = "SHIFT|CTRL", action = act.CopyTo "Clipboard" },
    { key = "V", mods = "SHIFT|CTRL", action = act.PasteFrom "Clipboard" },
    { key = "F", mods = "SHIFT|CTRL", action = act.Search "CurrentSelectionOrEmptyString" },

    { key = "L", mods = "SHIFT|CTRL", action = act.ShowDebugOverlay },

    -- Misc Wezterm features
    { key = "N", mods = "SHIFT|CTRL", action = act.SpawnWindow },
    { key = "P", mods = "SHIFT|CTRL", action = act.ActivateCommandPalette },
    { key = "U", mods = "SHIFT|CTRL", action = act.CharSelect { copy_on_select = true, copy_to = "ClipboardAndPrimarySelection" } },
    { key = "o", mods = "ALT",        action = act.ShowLauncher },
    { key = "o", mods = "ALT|SHIFT",  action = act.ShowLauncherArgs { flags = "WORKSPACES" } },
    {
        key = "n",
        mods = "ALT|SHIFT",
        action = act.PromptInputLine {
            description = "Enter workspace name",
            action = wezterm.action_callback(function(window, pane, line)
                if line then
                    window:perform_action(act.SwitchToWorkspace({ name = line }), pane)
                end
            end),
        }
    },
    -- Open URL
    {
        key = "O",
        mods = "CTRL|SHIFT",
        action = act.QuickSelectArgs {
            label = "open url",
            patterns = {
                "https?://\\S+",
            },
            action = wezterm.action_callback(function(window, pane)
                local url = window:get_selection_text_for_pane(pane)
                window:perform_action(act.ClearSelection, pane)
                wezterm.log_info("opening: " .. url)
                wezterm.open_with(url)
            end),
        },
    },



    { key = "c",      mods = "SHIFT|CTRL", action = act.CopyTo "Clipboard" },
    { key = "f",      mods = "SHIFT|CTRL", action = act.Search "CurrentSelectionOrEmptyString" },
    { key = "p",      mods = "SHIFT|CTRL", action = act.ActivateCommandPalette },

    { key = "Insert", mods = "SHIFT",      action = act.PasteFrom "PrimarySelection" },
    { key = "Copy",   mods = "NONE",       action = act.CopyTo "Clipboard" },
    { key = "Paste",  mods = "NONE",       action = act.PasteFrom "Clipboard" },
}

config.key_tables = {
    search_mode = {
        { key = "Enter",     mods = "NONE", action = act.CopyMode "PriorMatch" },
        { key = "Escape",    mods = "NONE", action = act.CopyMode "Close" },
        { key = "n",         mods = "CTRL", action = act.CopyMode "NextMatch" },
        { key = "p",         mods = "CTRL", action = act.CopyMode "PriorMatch" },
        { key = "r",         mods = "CTRL", action = act.CopyMode "CycleMatchType" },
        { key = "u",         mods = "CTRL", action = act.CopyMode "ClearPattern" },
        { key = "PageUp",    mods = "NONE", action = act.CopyMode "PriorMatchPage" },
        { key = "PageDown",  mods = "NONE", action = act.CopyMode "NextMatchPage" },
        { key = "UpArrow",   mods = "NONE", action = act.CopyMode "PriorMatch" },
        { key = "DownArrow", mods = "NONE", action = act.CopyMode "NextMatch" },
    },
}

return config
