local keymap = {}

local map = vim.keymap.set

vim.g.mapleader = " "

-- Open netrw
map("n", "<leader>p", ":Ex<CR>", {})

-- Use vim indention indent file
map("n", "g=", "gg=G''")

-- Keep scroll centered
map("n", "<C-d>", "<C-d>zz", {})
map("n", "<C-u>", "<C-u>zz", {})

-- Move selected lines
map("v", "J", ":m '>+1<CR>gv=gv")
map("v", "K", ":m '<-2<CR>gv=gv")

-- Un-highlight search terms
map("n", "<Esc>", ":noh<CR>", { silent = true })

-- spellcheck key
map("i", "<C-l>", "<c-g>u<Esc>[s1z=`]a<c-g>u", {})

map("n", "<leader>g", ":Neogit\n", {})
--map("n", "<leader>g", ":Git\n", {})

-- fzf keymap
map("n", "<Leader>ft", ":Telescope\n", {})
map("n", "<Leader>ff", ":Telescope find_files\n", {})
map("n", "<Leader>fb", ":Telescope buffers\n", {})
map("n", "<Leader>fg", ":Telescope live_grep\n", {})
map("n", "<Leader>fh", ":Telescope oldfiles\n", {})
map("n", "<Leader><C-r>", ":Telescope command_history\n", {})

-- lsp bindings
keymap.lsp = {}
keymap.lsp.declaration = "gD"
keymap.lsp.definition = "gd"
keymap.lsp.hover = "K"
keymap.lsp.implementation = "gi"
keymap.lsp.signature_help = "<C-k>"
keymap.lsp.add_workspace_folder = "<Leader>wa"
keymap.lsp.remove_workspace_folder = "<Leader>wr"
keymap.lsp.list_workspace_folders = "<Leader>wl"
keymap.lsp.type_definition = "<Leader>D"
keymap.lsp.rename = "<Leader>rn"
keymap.lsp.code_action = "<Leader>ca"
keymap.lsp.references = "gr"
keymap.lsp.line_diagnostics = "<Leader>e"
keymap.lsp.goto_prev = "[d"
keymap.lsp.goto_next = "]d"
keymap.lsp.set_loclist = "<Leader>q"
keymap.lsp.formatting = "<Leader>lf"

-- cmp bindings
map("i", "<M-Space>", function() require("cmp").complete() end, {})
keymap.cmp = {}
keymap.cmp.scroll_down = "<M-e>"
keymap.cmp.scroll_up = "<M-y>"
keymap.cmp.abort = "<C-e>"
keymap.cmp.confirm = "<CR>"

-- treesitter maps
keymap.ts = {}
keymap.ts.init_selection = "<Leader>ti"
keymap.ts.node_incremental = "<Leader>tn"
keymap.ts.scope_incremental = "<Leader>to"
keymap.ts.node_decremental = "<Leader>tp"

-- Harpoon
vim.keymap.set("n", "<leader>ho", function()
    require("harpoon.ui").toggle_quick_menu()
end, {})
vim.keymap.set("n", "<leader>hm", function()
    require("harpoon.mark").add_file()
end, {})
vim.keymap.set("n", "<leader><leader>", function()
    require("harpoon.ui").nav_file(vim.v.count)
end, {})
vim.keymap.set("n", "<C-h>", function()
    require("harpoon.ui").nav_file(1)
end, {})
vim.keymap.set("n", "<C-j>", function()
    require("harpoon.ui").nav_file(2)
end, {})
vim.keymap.set("n", "<C-k>", function()
    require("harpoon.ui").nav_file(3)
end, {})
vim.keymap.set("n", "<C-l>", function()
    require("harpoon.ui").nav_file(4)
end, {})

return keymap
