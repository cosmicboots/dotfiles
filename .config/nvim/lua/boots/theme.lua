-- setup base color theme
vim.g.dracula_colorterm = 1
vim.g.dracula_italic = 0
vim.g.tokyonight_style = "night"
vim.g.tokyonight_enable_italic = true
vim.g.tokyonight_transparent = true
vim.opt.termguicolors = true
--vim.cmd("colorscheme tokyonight")

-- show live find/replace results
vim.opt.inccommand = "split"

-- vim highlight settings
vim.cmd([[
highlight Comment cterm=italic gui=italic
highlight CursorLine ctermbg=238 cterm=none term=none
highlight CursorColumn ctermbg=238 cterm=none term=none
highlight Folded ctermfg=8
]])

vim.opt.cursorline = true
vim.opt.colorcolumn = "80"
vim.opt.listchars = "eol:⤶,trail:·,extends:»,precedes:«,tab:<->,leadmultispace:   ·"
vim.opt.showbreak = "⤷ "
vim.cmd("match ErrorMsg '\\s\\+$'")
vim.opt.list = true
