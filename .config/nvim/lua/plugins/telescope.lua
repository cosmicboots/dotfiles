return {
    {
        "nvim-telescope/telescope.nvim",
        cmd = "Telescope",
        dependencies = { "nvim-lua/plenary.nvim" }
    },
    {
        "cosmicboots/unicode_picker.nvim",
        dir = "/home/thom/sources/unicode_picker.nvim/",
        config = function()
            local unicode_picker = require("unicode_picker")
            vim.keymap.set("i", "<C-j>", unicode_picker.unicode_chars, {})
        end,
    }
}
