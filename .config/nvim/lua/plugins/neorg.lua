return {
    {
        "nvim-neorg/neorg",
        --tag = '*',
        lazy = true,
        ft = "norg",
        --keys = { "<leader>ni" },
        cmd = "Neorg",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-neorg/neorg-telescope",
        },
        config = function()
            vim.g.maplocalleader = " "
            require("neorg").setup({
                load = {
                        ["core.defaults"] = {},
                        ["core.dirman"] = {
                        config = {
                            workspaces = {
                                notes = "~/notes",
                            },
                            default_workspace = "notes",
                        },
                    },
                        ["core.journal"] = {
                        config = {
                            workspace = "notes"
                        }
                    },
                        ["core.concealer"] = {
                        config = {
                            dim_code_blocks = { enabled = false, },
                        },
                    },
                        ["core.export"] = {},
                        ["core.completion"] = {
                        config = {
                            engine = "nvim-cmp",
                        }
                    },
                        ["core.integrations.telescope"] = {},
                        ["core.qol.toc"] = {},
                },
            })
            local km = require("boots.keymap").neorg
        end
    }
}
