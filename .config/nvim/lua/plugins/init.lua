-- Misc things will go in here
return {
    { "mbbill/undotree",      cmd = "UndotreeShow" },
    { "rcarriga/nvim-notify", lazy = true },
    { "uga-rosa/ccc.nvim",    cmd = { "CccPick", "CccHighlighterEnable", "CccConvert" } },
    "tpope/vim-obsession",
    "tpope/vim-surround",
    "github/copilot.vim",
    --"jiangmiao/auto-pairs",
    { "ledger/vim-ledger", ft = "ledger" },
    "stevearc/vim-arduino",
    { "jbyuki/venn.nvim",  cmd = "VBox" },
    { "godlygeek/tabular", cmd = "Tabularize" },
}
