return {
    { "folke/trouble.nvim", cmd="Trouble", config = true },
    { "folke/todo-comments.nvim", cmd={"TodoTrouble", "TodoTelescope"}, config = true },
}
