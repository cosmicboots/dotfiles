return {
    { "dracula/vim", name = "dracula", lazy = true },
    {
        "folke/tokyonight.nvim",
        config = function()
            require("tokyonight").setup({
                style = "night",
                transparent = true,
                terminal_colors = true,
            })
        end,
        lazy = true
    },
    {
        "EdenEast/nightfox.nvim",
        lazy = true,
        opts = {
            options = {
                transparent = true,
                styles = {
                    comments = "italic",
                },
            }
        },
    },
    {
        "nvim-lualine/lualine.nvim",
        dependencies = { "kyazdani42/nvim-web-devicons" },
        config = function()
            require("lualine").setup({
                options = {
                    section_separators = "",
                    component_separators = "",
                },
                sections = {
                    lualine_c = {
                        {
                            "filename",
                            path = 1,
                        }
                    }
                }
            })
        end,
    }
}
