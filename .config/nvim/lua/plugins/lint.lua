return {
    "mfussenegger/nvim-lint",
    keys = {
        {
            "<leader>ll",
            function()
                require("lint").try_lint()
            end,
            desc = "Run linter",
        }
    },
    config = function()
        require('lint').linters_by_ft = {
            python = { "mypy", }
        }

        vim.api.nvim_create_autocmd({ "BufWritePost" }, {
            callback = function()
                require("lint").try_lint()
            end,
        })
    end,
}
