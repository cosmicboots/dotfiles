return {
    {
        "anuvyklack/hydra.nvim",
        config = function()
            local Hydra = require("hydra")
            Hydra({
                name = 'Side scroll',
                mode = 'n',
                body = 'z',
                heads = {
                    { 'h', '3zh' },
                    { 'l', '3zl', { desc = '←/→' } },
                    { 'H', 'zH' },
                    { 'L', 'zL',  { desc = 'half screen ←/→' } },
                }
            })

            local hint = [[
^^^ Arrow^^^^^^   Select region with <C-v>
^^^ ^ ^ _K_ ^ ^   _f_: surround it with box
^^^ _H_ ^ ^ _L_
^^^ ^ ^ _J_ ^ ^                      _<Esc>_]]

            Hydra({
                name = 'Draw Diagram',
                hint = hint,
                config = {
                    color = 'pink',
                    invoke_on_body = true,
                    hint = {
                        border = 'rounded'
                    },
                    on_enter = function()
                        vim.o.virtualedit = 'all'
                    end,
                },
                mode = 'n',
                body = '<leader>o',
                heads = {
                    { 'H',     '<C-v>h:VBox<CR>' },
                    { 'J',     '<C-v>j:VBox<CR>' },
                    { 'K',     '<C-v>k:VBox<CR>' },
                    { 'L',     '<C-v>l:VBox<CR>' },
                    { 'f',     ':VBox<CR>',      { mode = 'v' } },
                    { '<Esc>', nil,              { exit = true } },
                }
            })

            -- Debugging keys
            Hydra({
                name = "DAP",
                hint = [[_b_ - toggle breakpoint
_c_ - continue
_n_ - next
_s_ - step in
_S_ - step out
_C_ - clear breakpoints
_r_ - open REPL]],
                config = {
                    color = "pink",
                    invoke_on_body = true,
                    hint = {
                        border = "rounded",
                        position = "top-right",
                    },
                    on_enter = function()
                        -- Load DAP configs
                        require("mason-nvim-dap")
                    end,
                },
                mode = "n",
                body = "<leader>d",
                heads = {
                    { "b",     ":lua require'dap'.toggle_breakpoint()<CR>" },
                    { "c",     ":lua require'dap'.continue()<CR>" },
                    { "C",     ":lua require'dap'.clear_breakpoints()<CR>" },
                    { "n",     ":lua require'dap'.step_over()<CR>" },
                    { "s",     ":lua require'dap'.step_into()<CR>" },
                    { "S",     ":lua require'dap'.step_out()<CR>" },
                    { "r",     ":lua require'dap'.repl.toggle()<CR>" },
                    { "<Esc>", nil,                                        { exit = true } }
                },
            })
        end
    },
}
