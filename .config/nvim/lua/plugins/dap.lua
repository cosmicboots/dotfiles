return {
    {
        "jay-babu/mason-nvim-dap.nvim",
        dependencies = {
            "williamboman/mason.nvim",
            "mfussenegger/nvim-dap",
        },
        lazy = true,
        config = function()
            require("mason").setup()
            require("mason-nvim-dap").setup({
                handlers = {}
            })
        end
    },
    {
        "rcarriga/nvim-dap-ui",
        keys = {
            {
                "<leader>i",
                function()
                    require("dapui").toggle()
                end,
                desc = "launch dap-ui"
            }
        },
        config = function()
            require("dapui").setup({
                layouts = { {
                    elements = { {
                        id = "scopes",
                        size = 0.50
                    }, {
                        id = "breakpoints",
                        size = 0.25
                    }, {
                        id = "stacks",
                        size = 0.25
                    }, },
                    position = "left",
                    size = 40
                }, {
                    elements = { {
                        id = "repl",
                        size = 0.5
                    }, },
                    position = "bottom",
                    size = 10
                } },
            })
        end,
    }
}
