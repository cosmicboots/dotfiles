return {
    {
        "TimUntersberger/neogit",
        cmd = "Neogit",
        dependencies = "nvim-lua/plenary.nvim"
    },
    { "tpope/vim-fugitive", cmd = "Git" },
    {
        "lewis6991/gitsigns.nvim",
        config = true
    },
}
