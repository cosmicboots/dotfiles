return {
    "lukas-reineke/indent-blankline.nvim",
    enabled = false,
    opts = {
        show_current_context = true,
        show_current_context_start = true,
        use_treesitter = true,
    }
}
