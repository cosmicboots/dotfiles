local notes_dir = vim.fs.normalize("~/SecureGarden/Notes/obsidian/")
return {
    "epwalsh/obsidian.nvim",
    --lazy = true,
    event = { "BufReadPre " .. notes_dir .. "**.md" },
    keys = { {
        "<leader>nd",
        "<cmd>ObsidianToday<cr>",
        desc = "Open Today's Note"
    }, {
        "<leader>no",
        "<cmd>ObsidianOpen<cr>",
        desc = "Open note in obsidian"
    } },
    dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-telescope/telescope.nvim",
    },
    opts = {
        dir = notes_dir,
        daily_notes = {
            folder = "10_Daily Notes"
        },
        follow_url_func = function(url)
            vim.fn.jobstart({ "xdg-open", url })
        end,
        disable_frontmatter = true,
        note_frontmatter_func = function(note)
            local out = { id = note.id, tags = note.tags }
            if note.metadata ~= nil and require("obsidian").util.table_length(note.metadata) > 0 then
                for k, v in pairs(note.metadata) do
                    out[k] = v
                end
            end
            return out
        end,
        templates = {
            subdir = "templates",
        },
    },
    config = function(_, opts)
        require("obsidian").setup(opts)

        -- Optional, override the 'gf' keymap to utilize Obsidian's search functionality.
        -- see also: 'follow_url_func' config option below.
        vim.keymap.set("n", "gf", function()
            if require("obsidian").util.cursor_on_markdown_link() then
                return "<cmd>ObsidianFollowLink<CR>"
            else
                return "gf"
            end
        end, { noremap = false, expr = true })
    end,
}
