return {
    {
        "j-hui/fidget.nvim",
        tag = "legacy",
        event = "BufReadPre",
        config = function()
            require("fidget").setup({
                text = {
                    spinner = "dots",
                },
                window = {
                    blend = 0
                },
            })
        end
    },
    { "folke/neodev.nvim", opts = {} },
    {
        'VonHeikemen/lsp-zero.nvim',
        event = "BufReadPre",
        cmd = "Mason",
        dependencies = {
            -- LSP Support
            { 'neovim/nvim-lspconfig' },
            { 'williamboman/mason.nvim' },
            { 'williamboman/mason-lspconfig.nvim' },

            -- Autocompletion
            { 'hrsh7th/nvim-cmp' },
            { 'hrsh7th/cmp-buffer' },
            { 'hrsh7th/cmp-path' },
            { 'saadparwaiz1/cmp_luasnip' },
            { 'hrsh7th/cmp-nvim-lsp' },
            { 'hrsh7th/cmp-nvim-lua' },

            -- Snippets
            { 'L3MON4D3/LuaSnip' },
            -- Snippet Collection (Optional)
            { 'rafamadriz/friendly-snippets' },
        },
        config = function()
            local lsp = require('lsp-zero')
            lsp.preset('recommended')

            --lsp.ensure_installed({
            --    "rust_analyzer",
            --})

            local cmp = require("cmp")
            local km = require("boots.keymap").cmp
            local luasnip = require("luasnip")

            local cmp_mappings = lsp.defaults.cmp_mappings({
                [km.scroll_up] = cmp.mapping.scroll_docs(-1),
                [km.scroll_down] = cmp.mapping.scroll_docs(1),
                [km.abort] = cmp.mapping.abort(),
                [km.confirm] = cmp.mapping.confirm({ select = false }),
                -- go to next placeholder in the snippet
                ['<C-g>'] = cmp.mapping(function(fallback)
                    if luasnip.jumpable(1) then
                        luasnip.jump(1)
                    else
                        fallback()
                    end
                end, { 'i', 's' }),
                -- go to previous placeholder in the snippet
                ['<C-d>'] = cmp.mapping(function(fallback)
                    if luasnip.jumpable(-1) then
                        luasnip.jump(-1)
                    else
                        fallback()
                    end
                end, { 'i', 's' }),
            })

            -- "unmap" <Tab>
            cmp_mappings['<Tab>'] = nil

            lsp.setup_nvim_cmp({
                preselect = cmp.PreselectMode.None,
                completion = {
                    completeopt = 'menu,menuone,noinsert,noselect'
                },
                documentation = {
                    border = "double",
                    winhighlight = "Normal:NormalFloat,Search:None",
                },
                select_behavior = "insert",
                mapping = cmp_mappings,
            })

            local on_attach = function(client, bufnr)
                local opts = { buffer = bufnr, remap = false }
                local lsp_maps = require("boots.keymap").lsp

                vim.keymap.set("n", lsp_maps.declaration, function() vim.lsp.buf.declaration() end, opts)
                vim.keymap.set("n", lsp_maps.definition, function() vim.lsp.buf.definition() end, opts)
                vim.keymap.set("n", lsp_maps.hover, function() vim.lsp.buf.hover() end, opts)
                vim.keymap.set("n", lsp_maps.implementation, function() vim.lsp.buf.implementation() end, opts)
                vim.keymap.set("i", lsp_maps.signature_help, function() vim.lsp.buf.signature_help() end, opts)
                vim.keymap.set("n", lsp_maps.add_workspace_folder, function() vim.lsp.buf.add_workspace_folder() end,
                    opts)
                vim.keymap.set("n", lsp_maps.remove_workspace_folder,
                    function() vim.lsp.buf.remove_workspace_folder() end, opts)
                vim.keymap.set("n", lsp_maps.list_workspace_folders,
                    function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, opts)
                vim.keymap.set("n", lsp_maps.type_definition, function() vim.lsp.buf.type_definition() end, opts)
                vim.keymap.set("n", lsp_maps.rename, function() vim.lsp.buf.rename() end, opts)
                vim.keymap.set("n", lsp_maps.code_action, function() vim.lsp.buf.code_action() end, opts)
                vim.keymap.set("n", lsp_maps.references, function() vim.lsp.buf.references() end, opts)
                vim.keymap.set("n", lsp_maps.line_diagnostics, function() vim.diagnostic.open_float() end, opts)
                vim.keymap.set("n", lsp_maps.goto_prev, function() vim.diagnostic.goto_prev() end, opts)
                vim.keymap.set("n", lsp_maps.goto_next, function() vim.diagnostic.goto_next() end, opts)
                vim.keymap.set("n", lsp_maps.set_loclist, function() vim.lsp.diagnostic.set_loclist() end, opts)
                vim.keymap.set("n", lsp_maps.formatting, function() vim.lsp.buf.format({ async = true }) end, opts)
            end
            lsp.on_attach(on_attach)

            lsp.setup()
        end
    },
}
