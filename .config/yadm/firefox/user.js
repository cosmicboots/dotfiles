
// Disable about:config warning
user_pref("browser.aboutConfig.showWarning", false);

// Allow stylesheets
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

// Force JS popups to be tabs
user_pref("browser.link.open_newwindow.restriction", 0);

// Change ctrl+tab behavior
user_pref("browser.ctrlTab.sortByRecentlyUsed", true);

// Allow js results in URL bar
user_pref("browser.urlbar.filter.javascript", false);

// Enable browser config
user_pref("devtools.chrome.enabled", true);

// Disable clipboard eventss
user_pref("dom.event.clipboardevents.enabled", true);

// Reset interface scale
user_pref("layout.css.devPixelsPerPx", "-1");

// Disable kinetic scroll
user_pref("apz.gtk.kinetic_scroll.enabled", false);
