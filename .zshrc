# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=** r:|=**'
zstyle ':completion:*' max-errors 2 not-numeric
zstyle ':completion:*' menu select=10
zstyle ':completion:*' original true
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/thom/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory autocd
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

# Autoload zsh add-zsh-hook and vcs_info functions (-U autoload w/o substition, -z use zsh style)
autoload -Uz add-zsh-hook vcs_info
# Enable substitution in the prompt.
setopt prompt_subst
# Run vcs_info just before a prompt is displayed (precmd)
add-zsh-hook precmd vcs_info

# Enable checking for (un)staged changes, enabling use of %u and %c
zstyle ':vcs_info:*' check-for-changes true
# Set custom strings for an unstaged vcs repo changes (*) and staged changes (+)
zstyle ':vcs_info:*' unstagedstr ' *'
zstyle ':vcs_info:*' stagedstr ' +'
# Set the format of the Git information for vcs_info
zstyle ':vcs_info:git:*' formats       '(%b%u%c)'
zstyle ':vcs_info:git:*' actionformats '(%b|%a%u%c)'

# Set terminal window title
function xterm_title_precmd () {
    print -Pn -- '\e]2;%n@%m %~\a'
    [[ "$TERM" == 'screen'* ]] && print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-}\e\\'
}

function xterm_title_preexec () {
    print -Pn -- '\e]2;%n@%m %~ %# ' && print -n -- "${(q)1}\a"
    [[ "$TERM" == 'screen'* ]] && { print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-} %# ' && print -n -- "${(q)1}\e\\"; }
}

if [[ "$TERM" == (alacritty*|gnome*|konsole*|putty*|rxvt*|screen*|tmux*|xterm*|kitty*) ]]; then
    add-zsh-hook -Uz precmd xterm_title_precmd
    add-zsh-hook -Uz preexec xterm_title_preexec
fi

# Set up z - jump around to quickly change dirs
[[ -r "/usr/share/z/z.sh" ]] && source /usr/share/z/z.sh

# Get SSH status function
function get_ssh () {
	if [[ $SSH_CLIENT ]]; then
		echo -n "🔥"
	else
		echo -n "💧"
	fi
}

# Set up a command timer
_command_time_preexec() {
  timer=${timer:-$SECONDS}
  ZSH_COMMAND_TIME_MSG=${ZSH_COMMAND_TIME_MSG-"Command took: %s"}
  ZSH_COMMAND_TIME_COLOR=${ZSH_COMMAND_TIME_COLOR-"yellow"}
  export ZSH_COMMAND_TIME=""
}

_command_time_precmd() {
  if [ $timer ]; then
    timer_show=$(($SECONDS - $timer))
    if [ -n "$TTY" ] && [ $timer_show -ge ${ZSH_COMMAND_TIME_MIN_SECONDS:-3} ]; then
      export ZSH_COMMAND_TIME="$timer_show"
      if [ ! -z ${ZSH_COMMAND_TIME_MSG} ]; then
        zsh_command_time
      fi
    fi
    unset timer
  fi
}

zsh_command_time() {
  if [ -n "$ZSH_COMMAND_TIME" ]; then
    timer_show=$(printf '%dh:%02dm:%02ds\n' $(($ZSH_COMMAND_TIME/3600)) $(($ZSH_COMMAND_TIME%3600/60)) $(($ZSH_COMMAND_TIME%60)))
    #print -P "%F{$ZSH_COMMAND_TIME_COLOR}$(printf "${ZSH_COMMAND_TIME_MSG}\n" "$timer_show")%f"
  fi
}

precmd_functions+=(_command_time_precmd)
preexec_functions+=(_command_time_preexec)

ZSH_COMMAND_TIME_MIN_SECONDS=30

NL=$'\n'

PROMPT='%F{magenta}%T%f %F{red}%n%f%F{green}@%f%F{blue}%m%f %F{cyan}%~%f %F{green}${vcs_info_msg_0_}%f%F{cyan}%#%f '
PROMPT='%F{blue}%T%f %F{cyan}%~%f %F{green}${vcs_info_msg_0_}%f ${NL}%F{red}❱%f '
#RPROMPT="$(zsh_command_time)"
#RPROMPT="%F{white}%K{blue}%T%f%k"

alias copy="xclip -selection clipboard"
alias ls="exa"
alias ll="exa -lg --git"
alias la="exa -a"
alias send="rsync -avzP"
alias t="task"
alias rcal="rem -c -@,0 -w,0,1"
alias rcalw="rem -c+3 -@,0 -w,0,1"
alias now='date +"%FT%T%:z"'
# Setup fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh


# Set Terminal
export TERM=st-256color

# Set EDITOR
export EDITOR=nvim

# Set BROWSER
export BROWSER=firefox

# Set man pager
export MANPAGER='nvim +Man!'

# Using gpg-agent for ssh auth
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent

# Android Dev
export ANDROID_HOME="$HOME/Android/Sdk/"
export PATH="$PATH:$ANDROID_HOME/cmdline-tools/latest/bin:$ANDROID_HOME/platform-tools:$ANDROID_HOME/emulator"

# Add .local/bin to path
export PATH="$PATH:$HOME/.local/bin:$HOME/.cargo/bin"

# Remind path
export DOTREMINDERS="$HOME/.reminders/main.rem"

# hledger setup
export LEDGER_FILE="$HOME/finance/main.journal"

# direnv
eval "$(direnv hook zsh)"

eval "$(starship init zsh)"

# opam configuration
[[ ! -r /home/thom/.opam/opam-init/init.zsh ]] || source /home/thom/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null
eval $(opam env)
