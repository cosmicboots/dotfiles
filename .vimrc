" The Arch Linux global vimrc - setting only a few sane defaults
"
" DO NOT EDIT THIS FILE. IT'S OVERWRITTEN UPON UPGRADES.
"
" Use /etc/vimrc for system-wide and $HOME/.vimrc for personal configuration
" (for details see ':help initialization').
"
" Use :help '<option>' to see the documentation for the given option.

" Use Vim defaults instead of 100% vi compatibility
" Avoid side-effects when nocompatible has already been set.
if &compatible
  set nocompatible
endif

set backspace=indent,eol,start
set ruler
set suffixes+=.aux,.bbl,.blg,.brf,.cb,.dvi,.idx,.ilg,.ind,.inx,.jpg,.log,.out,.png,.toc
set suffixes-=.h
set suffixes-=.obj
set expandtab
set tabstop=4
set scrolloff=7
set smarttab
set autoindent
set shiftwidth=4
set number
set hlsearch
set ignorecase
set smartcase
hi Search ctermfg=0 ctermbg=14
"hi Visual ctermbg=18
hi Visual ctermbg=29
hi SpellBad ctermfg=0 ctermbg=13

" Quick spellcheck bind
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

set path+=**
set wildmenu

syntax on
syntax enable
filetype plugin indent on

" Move temporary files to a secure location to protect against CVE-2017-1000382
if exists('$XDG_CACHE_HOME')
  let &g:directory=$XDG_CACHE_HOME
else
  let &g:directory=$HOME . '/.cache'
endif
if has('persistent_undo')
    let &g:undodir=&g:directory . '/vim/undo//'
    set undofile
endif
let &g:backupdir=&g:directory . '/vim/backup//'
let &g:directory.='/vim/swap//'
" Create directories if they doesn't exist
if ! isdirectory(expand(&g:directory))
  silent! call mkdir(expand(&g:directory), 'p', 0700)
endif
if ! isdirectory(expand(&g:backupdir))
  silent! call mkdir(expand(&g:backupdir), 'p', 0700)
endif
if ! isdirectory(expand(&g:undodir))
  silent! call mkdir(expand(&g:undodir), 'p', 0700)
endif

" Make shift-insert work like in Xterm
if has('gui_running')
  map <S-Insert> <MiddleMouse>
  map! <S-Insert> <MiddleMouse>
endif

" Change cursor style
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[2 q"
let &t_EI = "\<Esc>[4 q"

" Add tab mappings
map <F7> :tabp<Enter>
map <F8> :tabn<Enter>

" Use Italic for comments
highlight Comment cterm=italic gui=italic
highlight CursorLine ctermbg=238 cterm=none term=none
highlight CursorColumn ctermbg=238 cterm=none term=none
highlight ColorColumn ctermbg=238 cterm=none term=none
highlight Folded ctermfg=8

" Cursorline settings
set cursorline
"set cursorlineopt=screenline,number

set colorcolumn=80

" Set white space chars
set listchars=eol:⏎,space:·

"" Markdown settings
" Auto set markdown type
autocmd BufNewFile,BufRead *.md set filetype=markdown
" Make it easier to navigate long lines
autocmd FileType markdown nnoremap <buffer> j gj
autocmd FileType markdown nnoremap <buffer> k gk
" Hide markdown formatting chars
autocmd FileType markdown set conceallevel=2
" Break line after word rather than in the middle of one
autocmd FileType markdown set linebreak

